#include "SPI.h"


void Gpio_init(){
	//sck = PA5
	//MISO = PA6
	//MOSI = PA7
	RCC ->  AHBENR |= RCC_AHBENR_GPIOAEN;
	//Alternate Function Mode
	GPIOA -> MODER |= (GPIO_MODER_MODER5_1)|(GPIO_MODER_MODER6_1)|(GPIO_MODER_MODER7_1);
	//Output push-pull
	GPIOA -> OTYPER &= ~(GPIO_OTYPER_OT_5);
	GPIOA -> OTYPER &= ~(GPIO_OTYPER_OT_6);
	GPIOA -> OTYPER &= ~(GPIO_OTYPER_OT_7);
	//40 MHz High speed
	GPIOA -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5;
	GPIOA -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR6;
	GPIOA -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR7;
	//No pull-up, pull-down
	GPIOA -> PUPDR &= ~(GPIO_PUPDR_PUPDR5);
	GPIOA -> PUPDR &= ~(GPIO_PUPDR_PUPDR6);
	GPIOA -> PUPDR &= ~(GPIO_PUPDR_PUPDR7);

}
void spi_init(){
	Gpio_init();
	//SPI Control Register (SPI_CR1)
	// SPI1 Configuration
	RCC -> APB2ENR |= RCC_APB2ENR_SPI1EN;
	// Make STM as master
	SPI1 -> CR1 |= SPI_CR1_MSTR;
	// baud Rate
	SPI1 -> CR1 |= SPI_CR1_BR_0;
	// data format 8 bit
	SPI1 -> CR1 &= ~(SPI_CR1_DFF);
	// SPI Enabl
	SPI1 -> CR1 |= SPI_CR1_SPE;
	SPI1 -> CR1 |= (SPI_CR1_SSM);
	SPI1 -> CR1 |= (SPI_CR1_SSI);
	// Full duplex
	SPI1 -> CR1 &= ~(SPI_CR1_BIDIMODE);
	// Idle on LOW
	SPI1 -> CR1 &= ~(SPI_CR1_CPOL);
	// first edge and rising riding data capture
	SPI1 -> CR1 &= ~(SPI_CR1_CPHA);

	//SPI Status Register (SPI_SR)


}
unsigned char send_Data(unsigned char data){
	SPI1 -> DR = data;
	while( !(SPI1->SR & SPI_FLAG_TXE) ); // wait until transmit complete
	while( !(SPI1->SR & SPI_FLAG_RXNE) ); // wait until receive complete
	while( SPI1->SR & SPI_FLAG_BSY ); // wait until SPI is not busy anymore
	return SPI1->DR; // return received data from SPI data register
}
