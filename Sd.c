#include "Sd.h"
#include "SPI.h"

void initChipSelect(){
	//SS sdCard = PC8
	RCC -> AHBENR |= RCC_AHBENR_GPIOCEN;
	GPIOC -> MODER |= (GPIO_MODER_MODER8_0);
	GPIOC -> OTYPER &= ~(GPIO_OTYPER_OT_8);
	GPIOC -> OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR8);
	GPIOC -> PUPDR &= ~(GPIO_PUPDR_PUPDR8);

}
void ChipSelectEnable(){
	GPIOC -> BSRR |= GPIO_BSRR_BR_8;
}
void ChipSelectDisable(){
	GPIOC -> BSRR |= GPIO_BSRR_BS_8;
}
void SD_command(unsigned char cmd, unsigned long arg,unsigned char crc, unsigned char read){
   unsigned char i, buffer[8];

   ChipSelectEnable();
   send_Data(cmd);
   send_Data(arg>>24);
   send_Data(arg>>16);
   send_Data(arg>>8);
   send_Data(arg);
   send_Data(crc);

   for(i=0; i<read; i++){
	   buffer[i] = send_Data(0xFF);
   }
	ChipSelectDisable();
}
